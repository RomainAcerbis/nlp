from french_lefff_lemmatizer.french_lefff_lemmatizer import FrenchLefffLemmatizer

class grammarParser():

    def separateSentences(self, inputText):
        separators = [",", "?", "!", ".", ";", "-", "'"]
        wordSeparators = ["mais", "ou", "et", "donc", "or", "car"]
        questions = []
        sentences = []
        newSentences = []

        splittedBySpace = str(inputText).split(" ")
        splittedBySpace[0] = splittedBySpace[0].lower()
        #print(splittedBySpace)
        for i in splittedBySpace:
            found = False
            question = False
            for sep in separators:
                if sep in i:
                    containsSep = i.split(sep)
                    if containsSep[0]:
                        newSentences.append(containsSep[0])
                    if sep == "?":
                        question = True
                    if sep != "-" and sep != "'":
                        sentences.append(newSentences)
                        questions.append(question)
                        newSentences = []
                    if containsSep[1]:
                        newSentences.append(containsSep[1])
                    found = True
            if not found:
                for sep in wordSeparators:
                    if sep == i.lower():
                        sentences.append(newSentences)
                        questions.append(question)
                        newSentences = []

            if not found:
                newSentences.append(i)
        if newSentences:
            sentences.append(newSentences)
        #print("s : " + str(sentences))7
        return sentences, questions

    def checkType(self, type, lemmatized):
        if len(lemmatized) != 0:
            for w in lemmatized:
                if w[1] == type:
                    #print(str(type) + " : " + w[0])
                    return w[0]
            return False
        else:
            #print("your word is unknown : " + word)
            return False

    def isolateImportant(self, sentence, isQuestion):
        lemmatizer = FrenchLefffLemmatizer()
        pronouns = ["je", "tu", "il", "elle", "on", "nous", "vous", "ils", "elles","j"]
        questions = ["qu", "quoi", "qui", "quand", "où", "comment", "quelle", "quel"]
        negations = ["n", "non", "ne", "ni", "pas"]
        negation = False
        question = False
        important = []
        verbs = []
        ncs = []
        nps = []
        dets = []
        adjs = []
        pns = []
        for i in range(len(sentence)):
            lemmatized = lemmatizer.lemmatize(sentence[i], "all")
            aux = False
            if lemmatized:
                if isQuestion:
                    question = True
                if sentence[i].lower() == "j":
                    pns.append(["je"])
                    pns.append(i)
                elif sentence[i].lower() in negations:
                    negation = True
                elif sentence[i].lower() in questions:
                    question = True
                elif self.checkType("adj", lemmatized) and sentence[i] in pronouns:
                    pns.append(self.checkType("adj", lemmatized))
                    pns.append(i)
                elif self.checkType("adj", lemmatized):
                    adjs.append(self.checkType("adj", lemmatized))
                    adjs.append(i)
                elif self.checkType("det", lemmatized):
                    dets.append(self.checkType("det", lemmatized))
                    dets.append(i)
                elif self.checkType("v", lemmatized) and i != (len(sentence) - 1):
                    if (self.checkType("auxAvoir", lemmatized) and self.checkType("v", lemmatizer.lemmatize(sentence[i+1], "all"))) or (self.checkType("auxEtre", lemmatized) and self.checkType("v", lemmatizer.lemmatize(sentence[i+1], "all"))):
                        aux = True
                    if not aux:
                        verbs.append(self.checkType("v", lemmatized))
                        verbs.append(i)
                elif self.checkType("nc", lemmatized):
                    ncs.append(self.checkType("nc", lemmatized))
                    ncs.append(i)
                elif self.checkType("np", lemmatized) and not self.checkType("cln", lemmatized):
                    nps.append(sentence[i])
                    nps.append(i)
        important.append(pns)
        important.append(verbs)
        #important.append(dets)
        important.append(ncs)
        important.append(nps)
        important.append(adjs)
        important.append(negation)
        important.append(question)
        #print(important)
        return important

    def getAllImportant(self, textInput):
        sentences, questions = self.separateSentences(textInput)
        importants = []
        for i in range(len(sentences)):
            importants.append(self.isolateImportant(sentences[i], questions[i]))
        return importants  # [sentence[type[value/index]]]

